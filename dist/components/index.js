"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
Object.defineProperty(exports, "TodaysDate", {
  enumerable: true,
  get: function get() {
    return _TodaysDate["default"];
  }
});

var _TodaysDate = _interopRequireDefault(require("./TodaysDate"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }
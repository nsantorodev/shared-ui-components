"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
Object.defineProperty(exports, "twoFactorAuthentication", {
  enumerable: true,
  get: function get() {
    return _twoFactorAuthentication["default"];
  }
});

var _twoFactorAuthentication = _interopRequireDefault(require("./twoFactorAuthentication"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }
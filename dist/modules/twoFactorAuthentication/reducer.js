"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _immutable = require("immutable");

var _actions = require("./actions");

var InitialState = new _immutable.Record({
  qrCodeUrl: '',
  secret: '',
  errors: [],
  status: 'default'
});

var _default = function _default() {
  var state = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : new InitialState();

  var _ref = arguments.length > 1 ? arguments[1] : undefined,
      type = _ref.type,
      payload = _ref.payload;

  switch (type) {
    case _actions.SET_MFA_DETAILS:
      {
        return state.set('qrCodeUrl', payload['mfa-url']).set('secret', payload.secret).set('errors', []).set('status', 'default');
      }

    case _actions.SET_ENABLE_ERROR:
      {
        return state.set('errors', payload);
      }

    case _actions.SET_ENABLE_SUCCESS:
      {
        return state.set('status', payload).set('errors', []).set('qrCodeUrl', '').set('secret', '');
      }

    case _actions.RESET_DETAILS:
      {
        return state.set('status', 'default').set('errors', []).set('qrCodeUrl', '').set('secret', '');
      }

    default:
      {
        return state;
      }
  }
};

exports["default"] = _default;
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.requestMFA = requestMFA;
exports.enableMFA = enableMFA;
exports.disableMFA = disableMFA;

function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance"); }

function _iterableToArrayLimit(arr, i) { if (!(Symbol.iterator in Object(arr) || Object.prototype.toString.call(arr) === "[object Arguments]")) { return; } var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(source, true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(source).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var OPTIONS = {
  credentials: 'same-origin',
  headers: {
    Accept: 'application/json',
    'Content-Type': 'application/json',
    'X-Id-Type': 'String'
  }
};
/**
 * call stream API
 * @param {string} eventId An Eddie event id
 */

function requestMFA() {
  var url = '/bpapi/rest/security/mfa';

  var options = _objectSpread({}, OPTIONS, {
    method: 'POST'
  });

  return fetch(url, options).then(function (response) {
    return Promise.all([response, response.json()]);
  }).then(function (_ref) {
    var _ref2 = _slicedToArray(_ref, 2),
        response = _ref2[0],
        json = _ref2[1];

    if (!response.ok) {
      return Promise.reject(response);
    }

    return json;
  })["catch"](function (err) {
    // eslint-disable-next-line no-console
    console.error('Error fetching MFA data.', err);
  });
}

function enableMFA(code) {
  var url = '/bpapi/rest/security/mfa';

  var options = _objectSpread({}, OPTIONS, {
    method: 'PUT',
    body: JSON.stringify({
      'mfa-code': code
    })
  });

  return fetch(url, options).then(function (response) {
    return Promise.resolve(response);
  }).then(function (response) {
    if (!response.ok) {
      return response.json();
    }

    return response.status;
  })["catch"](function (error) {
    // eslint-disable-next-line no-console
    console.error('Error enabling mfa', error);
  });
}

function disableMFA() {
  var url = '/bpapi/rest/security/mfa';

  var options = _objectSpread({}, OPTIONS, {
    method: 'DELETE'
  });

  return fetch(url, options).then(function (response) {
    return Promise.resolve(response);
  })["catch"](function (response) {
    // eslint-disable-next-line no-console
    console.error('Error Disabling 2FA', response);
  });
}
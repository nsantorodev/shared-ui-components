"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = root;

var _effects = require("redux-saga/effects");

var _actions = require("./actions");

var api = _interopRequireWildcard(require("./api"));

function _getRequireWildcardCache() { if (typeof WeakMap !== "function") return null; var cache = new WeakMap(); _getRequireWildcardCache = function _getRequireWildcardCache() { return cache; }; return cache; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } var cache = _getRequireWildcardCache(); if (cache && cache.has(obj)) { return cache.get(obj); } var newObj = {}; if (obj != null) { var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null; if (desc && (desc.get || desc.set)) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj["default"] = obj; if (cache) { cache.set(obj, newObj); } return newObj; }

var _marked =
/*#__PURE__*/
regeneratorRuntime.mark(watchMFADetailsRequest),
    _marked2 =
/*#__PURE__*/
regeneratorRuntime.mark(watchEnableMFARequest),
    _marked3 =
/*#__PURE__*/
regeneratorRuntime.mark(watchDisableMFARequest),
    _marked4 =
/*#__PURE__*/
regeneratorRuntime.mark(root);

function watchMFADetailsRequest() {
  var data;
  return regeneratorRuntime.wrap(function watchMFADetailsRequest$(_context) {
    while (1) {
      switch (_context.prev = _context.next) {
        case 0:
          if (!true) {
            _context.next = 20;
            break;
          }

          _context.next = 3;
          return (0, _effects.take)(_actions.GET_MFA_DETAILS);

        case 3:
          _context.prev = 3;
          _context.next = 6;
          return (0, _effects.call)(api.requestMFA);

        case 6:
          data = _context.sent;
          _context.next = 9;
          return (0, _effects.put)((0, _actions.setMFADetails)(data));

        case 9:
          _context.next = 11;
          return (0, _effects.put)((0, _actions.setEnableError)([]));

        case 11:
          _context.next = 18;
          break;

        case 13:
          _context.prev = 13;
          _context.t0 = _context["catch"](3);
          _context.next = 17;
          return (0, _effects.put)((0, _actions.setMFAError)(_context.t0));

        case 17:
          // eslint-disable-next-line no-console
          console.warn('Error fetching MFA', _context.t0);

        case 18:
          _context.next = 0;
          break;

        case 20:
        case "end":
          return _context.stop();
      }
    }
  }, _marked, null, [[3, 13]]);
}

function watchEnableMFARequest() {
  var _ref, payload, code, response;

  return regeneratorRuntime.wrap(function watchEnableMFARequest$(_context2) {
    while (1) {
      switch (_context2.prev = _context2.next) {
        case 0:
          if (!true) {
            _context2.next = 24;
            break;
          }

          _context2.next = 3;
          return (0, _effects.take)(_actions.ENABLE_MFA);

        case 3:
          _ref = _context2.sent;
          payload = _ref.payload;
          _context2.prev = 5;
          code = payload.replace(/ /g, '');
          _context2.next = 9;
          return (0, _effects.call)(api.enableMFA, code);

        case 9:
          response = _context2.sent;

          if (!(response && response.errors)) {
            _context2.next = 15;
            break;
          }

          _context2.next = 13;
          return (0, _effects.put)((0, _actions.setEnableError)(response.errors));

        case 13:
          _context2.next = 17;
          break;

        case 15:
          _context2.next = 17;
          return (0, _effects.put)((0, _actions.setEnableSuccess)('done'));

        case 17:
          _context2.next = 22;
          break;

        case 19:
          _context2.prev = 19;
          _context2.t0 = _context2["catch"](5);
          // yield put(setMFAError(e));
          // eslint-disable-next-line no-console
          console.warn('Error enabling MFA', _context2.t0);

        case 22:
          _context2.next = 0;
          break;

        case 24:
        case "end":
          return _context2.stop();
      }
    }
  }, _marked2, null, [[5, 19]]);
}

function watchDisableMFARequest() {
  return regeneratorRuntime.wrap(function watchDisableMFARequest$(_context3) {
    while (1) {
      switch (_context3.prev = _context3.next) {
        case 0:
          if (!true) {
            _context3.next = 15;
            break;
          }

          _context3.next = 3;
          return (0, _effects.take)(_actions.DISABLE_MFA);

        case 3:
          _context3.prev = 3;
          _context3.next = 6;
          return (0, _effects.call)(api.disableMFA);

        case 6:
          _context3.next = 8;
          return (0, _effects.put)((0, _actions.resetDetails)());

        case 8:
          _context3.next = 13;
          break;

        case 10:
          _context3.prev = 10;
          _context3.t0 = _context3["catch"](3);
          // yield put(setMFAError(e));
          // eslint-disable-next-line no-console
          console.warn('Error disabling MFA', _context3.t0);

        case 13:
          _context3.next = 0;
          break;

        case 15:
        case "end":
          return _context3.stop();
      }
    }
  }, _marked3, null, [[3, 10]]);
}

function root() {
  return regeneratorRuntime.wrap(function root$(_context4) {
    while (1) {
      switch (_context4.prev = _context4.next) {
        case 0:
          _context4.next = 2;
          return (0, _effects.all)([(0, _effects.fork)(watchMFADetailsRequest), (0, _effects.fork)(watchEnableMFARequest), (0, _effects.fork)(watchDisableMFARequest)]);

        case 2:
        case "end":
          return _context4.stop();
      }
    }
  }, _marked4);
}
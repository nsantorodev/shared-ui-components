"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.getMFADetails = getMFADetails;
exports.setMFADetails = setMFADetails;
exports.setMFAError = setMFAError;
exports.enableMFA = enableMFA;
exports.setEnableError = setEnableError;
exports.setEnableSuccess = setEnableSuccess;
exports.disableMFA = disableMFA;
exports.resetDetails = resetDetails;
exports.RESET_DETAILS = exports.DISABLE_MFA = exports.SET_ENABLE_SUCCESS = exports.SET_ENABLE_ERROR = exports.ENABLE_MFA = exports.SET_MFA_ERROR = exports.SET_MFA_DETAILS = exports.GET_MFA_DETAILS = void 0;
var GET_MFA_DETAILS = 'twoFactorAuthentication/GET_MFA_DETAILS';
exports.GET_MFA_DETAILS = GET_MFA_DETAILS;
var SET_MFA_DETAILS = 'twoFactorAuthentication/SET_MFA_DETAILS';
exports.SET_MFA_DETAILS = SET_MFA_DETAILS;
var SET_MFA_ERROR = 'twoFactorAuthentication/SET_MFA_ERROR';
exports.SET_MFA_ERROR = SET_MFA_ERROR;
var ENABLE_MFA = 'twoFactorAuthentication/ENABLE_MFA';
exports.ENABLE_MFA = ENABLE_MFA;
var SET_ENABLE_ERROR = 'twoFactorAuthentication/SET_ENABLE_ERROR';
exports.SET_ENABLE_ERROR = SET_ENABLE_ERROR;
var SET_ENABLE_SUCCESS = 'twoFactorAuthentication/SET_ENABLE_SUCCESS';
exports.SET_ENABLE_SUCCESS = SET_ENABLE_SUCCESS;
var DISABLE_MFA = 'twoFactorAuthentication/DISABLE_MFA';
exports.DISABLE_MFA = DISABLE_MFA;
var RESET_DETAILS = 'twoFactorAuthentication/RESET_DETAILS';
exports.RESET_DETAILS = RESET_DETAILS;

function getMFADetails() {
  return {
    type: GET_MFA_DETAILS
  };
}

function setMFADetails(payload) {
  return {
    type: SET_MFA_DETAILS,
    payload: payload
  };
}

function setMFAError(payload) {
  return {
    type: SET_MFA_ERROR,
    payload: payload
  };
}

function enableMFA(payload) {
  return {
    type: ENABLE_MFA,
    payload: payload
  };
}

function setEnableError(payload) {
  return {
    type: SET_ENABLE_ERROR,
    payload: payload
  };
}

function setEnableSuccess(payload) {
  return {
    type: SET_ENABLE_SUCCESS,
    payload: payload
  };
}

function disableMFA(payload) {
  return {
    type: DISABLE_MFA,
    payload: payload
  };
}

function resetDetails() {
  return {
    type: RESET_DETAILS
  };
}
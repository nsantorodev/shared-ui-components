"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
Object.defineProperty(exports, "modules", {
  enumerable: true,
  get: function get() {
    return _modules["default"];
  }
});
Object.defineProperty(exports, "components", {
  enumerable: true,
  get: function get() {
    return _components["default"];
  }
});

var _modules = _interopRequireDefault(require("./modules"));

var _components = _interopRequireDefault(require("./components"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }
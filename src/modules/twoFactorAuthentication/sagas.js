import { all, put, call, fork, take } from 'redux-saga/effects';

import {
  GET_MFA_DETAILS,
  setMFADetails,
  setMFAError,
  ENABLE_MFA,
  setEnableError,
  setEnableSuccess,
  DISABLE_MFA,
  resetDetails
} from './actions';

import * as api from './api';

function* watchMFADetailsRequest() {
  while (true) {
    yield take(GET_MFA_DETAILS);
    try {
      const data = yield call(api.requestMFA);
      yield put(setMFADetails(data));
      yield put(setEnableError([]));
    } catch (e) {
      yield put(setMFAError(e));
      // eslint-disable-next-line no-console
      console.warn('Error fetching MFA', e);
    }
  }
}

function* watchEnableMFARequest() {
  while (true) {
    const { payload } = yield take(ENABLE_MFA);
    try {
      const code = payload.replace(/ /g, '');
      const response = yield call(api.enableMFA, code);
      if (response && response.errors) {
        yield put(setEnableError(response.errors));
      } else {
        yield put(setEnableSuccess('done'));
      }
    } catch (e) {
      // yield put(setMFAError(e));
      // eslint-disable-next-line no-console
      console.warn('Error enabling MFA', e);
    }
  }
}

function* watchDisableMFARequest() {
  while (true) {
    yield take(DISABLE_MFA);
    try {
      yield call(api.disableMFA);
      yield put(resetDetails());
    } catch (e) {
      // yield put(setMFAError(e));
      // eslint-disable-next-line no-console
      console.warn('Error disabling MFA', e);
    }
  }
}

export default function* root() {
  yield all([
    fork(watchMFADetailsRequest),
    fork(watchEnableMFARequest),
    fork(watchDisableMFARequest),
  ]);
}

export const GET_MFA_DETAILS = 'twoFactorAuthentication/GET_MFA_DETAILS';
export const SET_MFA_DETAILS = 'twoFactorAuthentication/SET_MFA_DETAILS';
export const SET_MFA_ERROR = 'twoFactorAuthentication/SET_MFA_ERROR';
export const ENABLE_MFA = 'twoFactorAuthentication/ENABLE_MFA';
export const SET_ENABLE_ERROR = 'twoFactorAuthentication/SET_ENABLE_ERROR';
export const SET_ENABLE_SUCCESS = 'twoFactorAuthentication/SET_ENABLE_SUCCESS';
export const DISABLE_MFA = 'twoFactorAuthentication/DISABLE_MFA';
export const RESET_DETAILS = 'twoFactorAuthentication/RESET_DETAILS';

export function getMFADetails() {
  return {
    type: GET_MFA_DETAILS,
  };
}

export function setMFADetails(payload) {
  return {
    type: SET_MFA_DETAILS,
    payload
  };
}

export function setMFAError(payload) {
  return {
    type: SET_MFA_ERROR,
    payload
  };
}

export function enableMFA(payload) {
  return {
    type: ENABLE_MFA,
    payload
  };
}

export function setEnableError(payload) {
  return {
    type: SET_ENABLE_ERROR,
    payload
  };
}

export function setEnableSuccess(payload) {
  return {
    type: SET_ENABLE_SUCCESS,
    payload
  };
}

export function disableMFA(payload) {
  return {
    type: DISABLE_MFA,
    payload
  };
}


export function resetDetails() {
  return {
    type: RESET_DETAILS
  };
}

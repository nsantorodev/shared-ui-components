import { Record } from 'immutable';

import {
  SET_MFA_DETAILS,
  SET_ENABLE_ERROR,
  SET_ENABLE_SUCCESS,
  RESET_DETAILS
} from './actions';

const InitialState = new Record({
  qrCodeUrl: '',
  secret: '',
  errors: [],
  status: 'default'
});

export default (state = new InitialState(), { type, payload }) => {
  switch (type) {
    case SET_MFA_DETAILS: {
      return state.set('qrCodeUrl', payload['mfa-url'])
      .set('secret', payload.secret)
      .set('errors', [])
      .set('status', 'default');
    }
    case SET_ENABLE_ERROR: {
      return state.set('errors', payload);
    }
    case SET_ENABLE_SUCCESS: {
      return state.set('status', payload)
      .set('errors', [])
      .set('qrCodeUrl', '')
      .set('secret', '');
    }
    case RESET_DETAILS: {
      return state.set('status', 'default')
      .set('errors', [])
      .set('qrCodeUrl', '')
      .set('secret', '');
    }
    default: {
      return state;
    }
  }
};

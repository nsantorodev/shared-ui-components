import * as actions from './actions';
import * as api from './api';
import reducer from './reducer';
import sagas from './sagas';

export default {
  actions,
  api,
  reducer,
  sagas
};

const OPTIONS = {
  credentials: 'same-origin',
  headers: {
    Accept: 'application/json',
    'Content-Type': 'application/json',
    'X-Id-Type': 'String'
  }
};

/**
 * call stream API
 * @param {string} eventId An Eddie event id
 */
export function requestMFA() {
  const url = '/bpapi/rest/security/mfa';
  const options = {
    ...OPTIONS,
    method: 'POST',
  };

  return fetch(url, options)
    .then(response => Promise.all([response, response.json()]))
    .then(([response, json]) => {
      if (!response.ok) {
        return Promise.reject(response);
      }
      return json;
    })
    .catch((err) => {
      // eslint-disable-next-line no-console
      console.error('Error fetching MFA data.', err);
    });
}

export function enableMFA(code) {
  const url = '/bpapi/rest/security/mfa';
  const options = {
    ...OPTIONS,
    method: 'PUT',
    body: JSON.stringify({ 'mfa-code': code }),
  };

  return fetch(url, options)
  .then(response => Promise.resolve(response))
  .then((response) => {
    if (!response.ok) {
      return response.json();
    }
    return response.status;
  })
  .catch((error) => {
      // eslint-disable-next-line no-console
    console.error('Error enabling mfa', error);
  });
}

export function disableMFA() {
  const url = '/bpapi/rest/security/mfa';
  const options = {
    ...OPTIONS,
    method: 'DELETE',
  };

  return fetch(url, options)
    .then(response => Promise.resolve(response))
    .catch((response) => {
      // eslint-disable-next-line no-console
      console.error('Error Disabling 2FA', response);
    });
}

const argv = require('yargs').argv;
const gulp = require('gulp');
const replace = require('gulp-replace');
const log = require('fancy-log');

// This task is only run as part of the `yarn version` lifecycle
gulp.task('version', () => {
  const getVersion = (str) => {
    const regex = /^[0-9]{1,2}\.[0-9]{1,2}\.[0-9]{1,2}/.exec(str);
    return regex ? regex[0] : '';
  };

  const date = new Date();
  const dateStr = [date.getFullYear(), date.getMonth() + 1, date.getDate()].join('-');

  const changelogUpdate = `[Unreleased]\n\n### [${getVersion(argv.newversion)}] - ${dateStr}`;
  log('Updating CHANGELOG.md');
  log('New version ready to be published by running: git push origin master `git describe`');

  return gulp.src(['./CHANGELOG.md'])
    .pipe(replace(/\[Unreleased\]/, changelogUpdate))
    .pipe(gulp.dest('.'));
});

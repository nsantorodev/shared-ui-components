# Changelog
All notable changes to this project will be documented in this file. This changelog has been added to include technical improvements that may be otherwise missed.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

### [0.2.8] - 2019-9-25

### [0.2.7] - 2019-9-25
- Added Gulp for versioning
- Added back `dist` folder for compiled files

## [0.2.6] - 2019-09-25
### Removed
- Removed prod compiled files from git

## [0.2.5] - 2019-09-25
### Removed
- Removed prepare command

## [0.2.4] - 2019-09-25
### Changed
- Moved deps to devDeps

## [0.2.3] - 2019-09-25
### Changed
- Updated build command to serve components at root level instead of `build` folder

### Added
- Added new components folder

## [0.2.2] - 2019-09-20
### Changed
- Fixed modules export

## [0.2.1] - 2019-09-20
### Changed
- added modules export

## [0.2.0] - 2019-09-20
### Added
- added twoFactorAuthentication module
